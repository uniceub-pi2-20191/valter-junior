import React from 'react';
import { createStackNavigator, createAppContainer,  createBottomTabNavigator, createDrawerNavigator, DrawerItems } from 'react-navigation';
import { ScrollView, View, Text, StyleSheet, TouchableOpacity, Image, Button} from 'react-native';


import SignUpScreen from './src/screens/signUpScreen.js';
import LoginScreen from './src/screens/loginScreen.js';
import MainScreen from './src/screens/mainScreen.js';
import CartScreen from './src/screens/cartScreen.js';

import BookScreen from './src/screens/books/bookScreen.js';
import BookScreen2 from './src/screens/books/bookScreen2.js';
import BookScreen3 from './src/screens/books/bookScreen3.js';

const customDrawerComponent = (props) => (
  <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.8)'}}>

    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <Image
        style={{width: 220, height: 120, borderRadius: 0}} 
        source={require('./src/images/icon.png')} 
      />
    </View>

    <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
      <Text style={{fontSize: 24, color: 'white', fontStyle: 'italic'}}>
        SeboMobile
      </Text>
    </View>

    <ScrollView>
      <DrawerItems {...props}/>
    </ScrollView>
  </View>
);



const RootStack = createStackNavigator(
  { // Those are the stack navigator screens
    SignUp: {
      screen: SignUpScreen,
    },
    Login: {
      screen: LoginScreen,
      navigationOptions: {
       header: null },
    },
    Book: {
      screen: BookScreen,
    },
    Book2: {
      screen: BookScreen2,
    },
    Book3: {
      screen: BookScreen3,
    },
    // These here are the drawer navigator screens
    Main: {
      navigationOptions: {
        header: null },
      screen: createDrawerNavigator({
        Main:{
          screen: MainScreen,
        },
        Cart:{
          screen: CartScreen
        },
      },
      {
        contentComponent: customDrawerComponent,
      }),
      // navigationOptions : {
      //   headerLeft: (
      //   <View style={{alignItems: 'flex-start', marginBottom: 10, marginTop: 10}}>
      //     <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
      //         <Image
      //           style={{width: 30, height: 30}} 
      //           source={require('./src/images/menu.png')}/>
      //     </TouchableOpacity>
      //   </View>)
      // }
    },

  },
  {
    initialRouteName: 'Login',

    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: 'rgba(0,0,0,0.8)',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
);



const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {  
  render() {
    return <AppContainer />;
  }
}
