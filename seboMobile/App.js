
import React, { Component } from 'react';
import {StyleSheet, Text, View, StatusBar} from 'react-native';
import SplashScreen from 'react-native-splash-screen';


export default class App extends Component {
  componentDidMount(){
    this.checkAuth();
  }

  checkAuth = () => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 5000);
  }
  
  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.8)',
  },
});