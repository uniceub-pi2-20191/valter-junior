import React from 'react';
import { ScrollView, View, Text, TextInput, StyleSheet, Alert, TouchableOpacity, Image, Button} from 'react-native';
import SplashScreen from 'react-native-splash-screen';




export default class mainScreen extends React.Component{
  constructor(props) {
    super(props);
  }
  componentDidMount(){
    this.checkAuth();
  }
  
  checkAuth = () => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 500);
  }

    render() {
      return (
        <View style={styles.container}>

          <View style={{alignItems: 'flex-start', marginBottom: 10, marginTop: 10}}>
            <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
                <Image
                  style={{width: 30, height: 30}} 
                  source={require('../images/menu.png')}/>
            </TouchableOpacity>
          </View>

          <View style={{alignItems: 'center', marginBottom: 20, marginTop: 20}}>
            <Text style={styles.text}> Best Sellers!</Text>
          </View>

          <ScrollView style={{flexDirection: 'row'}}>

            <View style={{flexDirection: 'row'}}>

              <TouchableOpacity style={styles.books} onPress={() => this.props.navigation.navigate('Book')}>
                <Image
                  style={{width: 120, height: 170}} 
                  source={require('../images/book.png')}/>
              </TouchableOpacity>

              <TouchableOpacity style={styles.books} onPress={() => this.props.navigation.navigate('Book2')}>
                <Image
                  style={{width: 120, height: 170}} 
                  source={require('../images/book2.png')}/>
              </TouchableOpacity>

              <TouchableOpacity style={styles.books} onPress={() => this.props.navigation.navigate('Book3')}>
                <Image
                  style={{width: 120, height: 170}} 
                  source={require('../images/book4.png')}/>
              </TouchableOpacity>

            </View>
          </ScrollView>  
      </View>
    );
  };
};

  const styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor: 'rgba(0,0,0,0.8)',
    },
    books:{
      // justifyContent:'space-around',
      // alignContent: 'space-around'
    },
    text:{
      fontSize: 24, 
      color: 'white',
      fontStyle: 'italic',
      marginRight: 35,
      justifyContent: 'center'
    },
  })