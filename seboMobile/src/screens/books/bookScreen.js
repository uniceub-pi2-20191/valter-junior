import React from 'react';
import { ScrollView, View, Text, Image, StyleSheet, Alert, TouchableOpacity} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {Platform} from 'react-native';
import firebase from 'react-native-firebase';
import { Row } from 'native-base';



export default class bookScreen extends React.Component{
  componentDidMount(){
    this.checkAuth();
  }

  checkAuth = () => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 500);
  }

    render() {
      return (
        <View style={styles.container}>
        <ScrollView >
          <View style={{flexDirection: 'row', marginLeft: 30,marginTop: 50, marginBottom:35}}>
            <Image style={{width: 120, height: 170}} source={require('/home/dewary/seboMobile/src/images/book.png')}/>

            <View style={{alignItems: 'center', marginLeft: 40}}>
              <Text style={{fontSize: 24, color: 'white',}}>Description</Text>
              <Text style={{fontSize: 14, color: 'white'}}>A book called</Text>
              <Text style={{fontSize: 14, color: 'white'}}>"A Grace of Crowns".</Text>
              <Text style={{fontSize: 14, color: 'white'}}>By Amanda Mae</Text>
            </View>

          </View>

          <View style={{alignItems:'center', margin: 15}}>
            <TouchableOpacity style={{backgroundColor:'#00BFFF', borderRadius: 50, width: 210, alignItems:'center'}}>
              <Text style={{fontSize: 18, color: 'white',}}>Buy</Text>
            </TouchableOpacity>
          </View>

          <View style={{alignItems:'center',}}>
            <TouchableOpacity style={{backgroundColor:'#50C878', borderRadius: 50, width: 210, alignItems:'center'}}>
              <Text style={{fontSize: 18, color: 'white',}}>Add to Cart</Text>
            </TouchableOpacity>
          </View>



          <View style={{alignItems:'center', margin: 20, marginTop:35}}>
            <Text style={{fontSize: 18, color: 'red',}}>Related</Text>
          </View>

          <ScrollView horizontal= {true}>
            <View style={{flexDirection: 'row',}}> 
              <Image style={{width: 120, height: 170, margin: 3}} source={require('/home/dewary/seboMobile/src/images/book2.png')}/>
              <Image style={{width: 120, height: 170, margin: 3}} source={require('/home/dewary/seboMobile/src/images/book4.png')}/>
              <Image style={{width: 120, height: 170, margin: 3}} source={require('/home/dewary/seboMobile/src/images/book2.png')}/>
              <Image style={{width: 120, height: 170, margin: 3}} source={require('/home/dewary/seboMobile/src/images/book4.png')}/>
              <Image style={{width: 120, height: 170, margin: 3}} source={require('/home/dewary/seboMobile/src/images/book2.png')}/>
              <Image style={{width: 120, height: 170, margin: 3}} source={require('/home/dewary/seboMobile/src/images/book4.png')}/>
            </View>
          </ScrollView>

        </ScrollView>  
      </View>
    );
  };
};

  const styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor: 'rgba(0,0,0,0.8)',
    },
  })