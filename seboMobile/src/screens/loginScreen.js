import React from 'react';
import { ScrollView, View, Text, TextInput, StyleSheet, Alert, Button, TouchableOpacity} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {Platform} from 'react-native';
import firebase from 'react-native-firebase'



export default class Login extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorMessage: null
    };
  }

  componentDidMount(){
    this.checkAuth();
  }

  checkAuth = () => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 2000);
  }

  checkData(){
    if ((this.state.email == '') || (this.state.password == '')){
      Alert.alert('Invalid Email or Password', `Email: ${this.state.email}\nPassword: ${this.state.password}`)
      }
      
      else if ((this.state.email.includes('@')) == false){
        Alert.alert('Invalid E-mail!', `Email: ${this.state.email}`)
      }

    else{
      firebase.auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => this.props.navigation.navigate('Main'))
      .catch(error => this.setState({ errorMessage: error.message }))
    }
  }

    render() {
      return (
        <View style={styles.container}>
          <ScrollView>

                <View style={styles.form}>

                  <Text style={styles.text}>Email</Text>
              
                  <View style={{marginBottom: 7}}>
                    <TextInput 
                        style={styles.textBox}
                        autoCapitalize= 'none'
                        onChangeText={(email) => this.setState({email})}/>
                  </View>
                  
                  <Text style={styles.text}>Password</Text>
                    
                  <View style={{marginBottom: 7}}>
                    <TextInput 
                        style={styles.textBox}
                        autoCapitalize='none'
                        secureTextEntry= {true}
                        onChangeText={(password) => this.setState({password})}/>
                  </View>

                  <View style={{paddingTop: 10}}>
                  <TouchableOpacity style={styles.login}
                    onPress={() => this.checkData()}>
                    <Text style={styles.signInText}>SignIn</Text>
                  </TouchableOpacity>

                  <View style={{alignItems:'center'}}>
                    <Text style={styles.textOr}>OR</Text>
                  </View>

                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity style={styles.loginFb}>
                      <Text style={styles.signInText}>FB</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.loginG}>
                    <Text style={styles.signInText}>Google</Text>
                  </TouchableOpacity>
                  </View>
                
                  <View style={{flexDirection: 'row', justifyContent: 'center', paddingTop: 30}}>
                    <Text style={styles.dntText}>Don't have an account?</Text>
                  
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('SignUp')}>
                      <Text style={styles.signUpText}>SignUp</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              
          </ScrollView>
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor: 'rgba(0,0,0,0.8)',
    },
    form:{
      flex: 1,
      marginTop: 50,
      padding: 60,
      justifyContent: 'center',
    },
    login:{
      flex:1, 
      borderRadius: 50,
      justifyContent:'space-around',
      alignItems:'center',
      backgroundColor: '#00BFFF',
      margin: 20,
      height: 40,
    },
    loginFb:{
      flex:1, 
      borderRadius: 50,
      justifyContent:'space-around',
      alignItems:'center',
      backgroundColor: '#3C5A99',
      margin: 20,
      height: 40,
    },
    loginG:{
      flex:1, 
      borderRadius: 50,
      justifyContent:'space-around',
      alignItems:'center',
      backgroundColor: '#F4B400',
      margin: 20,
      height: 40,
    },
    signInText:{
      fontSize: 16, 
      color: 'white',
      fontStyle: 'normal'
    },
    textOr:{
      fontSize: 14, 
      color: '#FFF',
      fontStyle: 'italic',
      paddingTop: 5,
      
    },
    signUpText:{
      fontSize: 14, 
      color: '#50C878',
      fontStyle: 'normal',
    },
    dntText:{
      fontSize: 14, 
      color: '#fff',
      fontStyle: 'italic',
      paddingRight: 7
    },
    sign:{
      flex:1, 
      borderRadius: 50,
    },
    text:{
      fontSize: 18, 
      color: 'white',
      fontStyle: 'normal', 
      marginBottom: 7
    },
    textBox:{
      color: 'white',
      fontSize: 18,
      height: 36,
      paddingVertical: Platform.OS === "ios" ? 7 : 0,
      paddingHorizontal: 7,
      borderRadius: 4,
      borderColor: 'rgba(255,255,255,0.4)',
      borderWidth: 1,
      marginBottom: 5
    },
  
  })