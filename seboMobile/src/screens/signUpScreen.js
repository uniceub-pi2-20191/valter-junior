import React from 'react';
import { ScrollView, View, Text, TextInput, StyleSheet, Alert, TouchableOpacity} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {Platform} from 'react-native';
import firebase from 'react-native-firebase';



export default class SignUp extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      rePassword: '',
      errorMessage: null,
    };
  }

  checkData(){
    if ((this.state.email == '') || (this.state.password == '')){
      Alert.alert('Invalid Email or Password', `Email: ${this.state.email}\nPassword: ${this.state.password}`)
      }
      
      else if ((this.state.email.includes('@')) == false){
        Alert.alert('Invalid E-mail!', `Email: ${this.state.email}`)
      }
      else if ((this.state.password != this.state.rePassword)){
        Alert.alert('Password did not match!')
      }

    else{
      firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => this.props.navigation.navigate('Login'))
      .catch(error => this.setState({ errorMessage: error.message }))
      Alert.alert("Cadastro concluido com sucesso!")
    }
  }
    

    render() {
      return (
        <View style={styles.container}>
        <ScrollView>

            <View style={styles.form}>
              <Text style={styles.text}>Email</Text>
            
              <View style={{marginBottom: 7}}>
                <TextInput 
                    style={styles.textBox}
                    autoCompleteType='email'
                    autoCapitalize='none'
                    onChangeText={(email) => this.setState({email})}/>
              </View>
              
                <Text style={styles.text}>Password</Text>
                
              <View style={{marginBottom: 7}}>
                <TextInput 
                    style={styles.textBox}
                    autoCapitalize='none'
                    secureTextEntry= {true}
                    onChangeText={(password) => this.setState({password})}/>
              </View>

              <Text style={styles.text}>Re-Password</Text>
                
              <View style={{marginBottom: 7}}>
                <TextInput 
                    style={styles.textBox}
                    autoCapitalize='none'
                    secureTextEntry= {true}
                    onChangeText={(rePassword) => this.setState({rePassword})}/>
              </View>

              <TouchableOpacity style={styles.sign} 
              onPress={() => this.checkData()}>
                <View>
                    <Text style={{fontSize: 18, color: 'white'}}>SignUp</Text>
                </View>
            </TouchableOpacity>
            </View>

        </ScrollView>  
      </View>
    );
  };
};

  const styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor: 'rgba(0,0,0,0.8)',
    },
    form:{
      flex: 1,
      marginTop: 50,
      padding: 60,
      justifyContent: 'center',
    },
    sign:{
      flex:1, 
      borderRadius: 50,
      justifyContent:'space-around',
      alignItems:'center',
      backgroundColor: '#50C878',
      margin: 20,
      height: 40,
    },
    textBox:{
      color: 'white',
      fontSize: 18,
      height: 36,
      paddingVertical: Platform.OS === "ios" ? 7 : 0,
      paddingHorizontal: 7,
      borderRadius: 4,
      borderColor: 'rgba(255,255,255,0.4)',
      borderWidth: 1,
      marginBottom: 5
    },
    text:{
      fontSize: 18, 
      color: 'white',
      fontStyle: 'normal', 
      marginBottom: 7
    },
  
  })