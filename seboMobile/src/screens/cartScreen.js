import React from 'react';
import { ScrollView, View, Text, StyleSheet, Alert, TouchableOpacity, Image} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {Platform} from 'react-native';
import firebase from 'react-native-firebase';



export default class cartScreen extends React.Component{
  componentDidMount(){
    this.checkAuth();
  }

  checkAuth = () => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 500);
  }

    render() {
      return (
        <View style={styles.container}>
          <ScrollView>
            <View style={{alignItems: 'flex-start', marginBottom: 10, marginTop: 10}}>
              <TouchableOpacity style={styles.books} onPress={() => this.props.navigation.toggleDrawer()}>
                  <Image
                    style={{width: 30, height: 30}} 
                    source={require('../images/menu.png')}/>
              </TouchableOpacity>
            </View>
          </ScrollView>  
      </View>
    );
  };
};

  const styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor: 'rgba(0,0,0,0.8)',
    },
  })